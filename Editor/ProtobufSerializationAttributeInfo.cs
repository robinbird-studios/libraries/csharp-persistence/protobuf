﻿#region Disclaimer

// <copyright file="ProtobufSerializationAttributeInfo.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>

#endregion

namespace RobinBird.Persistence.Protobuf.Editor
{
    using System;
    using Persistence.Editor.Interfaces;
    using ProtoBuf;

    /// <summary>
    /// Provides information about the Protobuf serializer
    /// </summary>
    public class ProtobufSerializationAttributeInfo : ISerializationAttributeInfo
    {
        #region ISerializationAttributeInfo Implementation

        public Type GetIgnoreAttributeType()
        {
            return typeof (ProtoIgnoreAttribute);
        }

        #endregion
    }
}