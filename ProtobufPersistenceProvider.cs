﻿#region Disclaimer
// <copyright file="ProtobufPersistenceProvider.cs">
// Copyright (c) 2016 - 2017 All Rights Reserved
// </copyright>
// <author>Robin Fischer</author>
#endregion

namespace RobinBird.Persistence.Protobuf
{
    using System;
    using System.IO;
    using ProtoBuf.Meta;
    using RobinBird.Persistence.Runtime;

    public class ProtobufPersistenceProvider : IPersistenceProvider
    {
        private RuntimeTypeModel typeModel;

        public ProtobufPersistenceProvider(RuntimeTypeModel typeModel)
        {
            this.typeModel = typeModel;
        }

        #region IPersistenceProvider Implementation
        public string Extension
        {
            get { return ".proto"; }
        }

        public void Serialize(Stream s, object o, Type t)
        {
            typeModel.Serialize(s, o);
        }

        public object Deserialize(Stream s, Type t)
        {
            return typeModel.Deserialize(s, null, t);
        }

        public string GetPathWithExtension(string path)
        {
            return Path.ChangeExtension(path, Extension);
        }
        #endregion
    }
}